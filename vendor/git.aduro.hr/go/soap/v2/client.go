package v2

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"sync"

	"golang.org/x/net/context"
)

// Logging function
type XmlLoggerFunc func(req, res []byte)

// call struct
type call struct {
	act string
	req *Envelope
	res *Envelope
	xrs []byte
	xrq []byte
	log bool
}

// Client represents an SOAP/HTTP Client.
type Client struct {
	method    string
	url       string
	httpc     *http.Client
	user      string
	pass      string
	xmlLogger XmlLoggerFunc
}

// NewClient returns a new Client which is a wrapper around
// http.Client to provide SOAP encoding/decoding and communication.
func NewClient(url string, trans *http.Transport) *Client {
	if trans == nil {
		trans = http.DefaultTransport.(*http.Transport)
		trans.DisableKeepAlives = false
		trans.DisableCompression = false
	}
	t := &Client{
		method: "POST",
		url:    url,
		httpc:  &http.Client{Transport: trans},
	}
	return t
}

func (t *Client) Auth(user, pass string) {
	t.user = user
	t.pass = pass
}

// Method returns http method used for communication
func (t *Client) Method() string {
	return t.method
}

// SetMethod sets http method used for communication
func (t *Client) SetMethod(m string) {
	t.method = m
}

// Set xml logger function
func (t *Client) SetXmlLogger(xmlLogger XmlLoggerFunc) {
	t.xmlLogger = xmlLogger
}

// Soap call
func (t *Client) call(c *call) error {
	b := &bytes.Buffer{}
	if err := xml.NewEncoder(b).Encode(c.req); err != nil {
		return err
	}
	req, err := http.NewRequest(t.method, t.url, b)
	if err != nil {
		return err
	}
	req.Close = false
	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	req.Header.Add("SOAPAction", c.act)
	if t.user != "" {
		req.SetBasicAuth(t.user, t.pass)
	}
	if c.log == true {
		c.xrq, _ = httputil.DumpRequest(req, true)
	}
	res, err := t.httpc.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if c.log == true {
		c.xrs, _ = httputil.DumpResponse(res, true)
	}
	switch res.StatusCode {
	case 200:
		// OK
	case 500:
		// Fault
	default:
		return fmt.Errorf("soap: unexpected HTTP response: %s", res.Status)
	}
	if res.ContentLength == 0 {
		return fmt.Errorf("soap: error document empty")
	}
	if err := xml.NewDecoder(res.Body).Decode(c.res); err != nil {
		return err
	}
	ioutil.ReadAll(res.Body)
	return nil
}

// CallLog invokes SOAP method call, waits for it to complete, and returns its error status.
func (t *Client) Call(action string, request, response *Envelope) error {
	log := false
	if t.xmlLogger != nil {
		log = true
	}
	c := &call{
		act: action,
		req: request,
		res: response,
		log: log,
	}
	err := t.call(c)
	if t.xmlLogger != nil {
		go t.xmlLogger(c.xrq, c.xrs)
	}
	return err
}

// CallLog invokes SOAP method call, waits for it to complete, and returns its error status
// and raw http request and response.
func (t *Client) CallLog(action string, request, response *Envelope) (error, []byte, []byte) {
	c := &call{
		act: action,
		req: request,
		res: response,
		log: true,
	}
	err := t.call(c)
	return err, c.xrq, c.xrs
}

var (
	buffPool = sync.Pool{
		New: func() interface{} { return bytes.NewBuffer(nil) },
	}
	transPool = sync.Pool{
		New: func() interface{} { return &http.Transport{} },
	}
)

// CtxClient represents  context aware SOAP/HTTP client.
type CtxClient struct {
	trans *http.Transport
	url   string
	user  string
	pass  string
}

// NewCtxClient returns a new CtxClient which is a wrapper around
// http.Client to provide SOAP encoding/decoding and communication.
func NewCtxClient(url string, trans *http.Transport) *CtxClient {
	t := &CtxClient{
		trans: trans,
		url:   url,
	}
	if t.trans != nil {
		transPool = sync.Pool{
			New: t.transPool,
		}
	}
	return t
}

func (t *CtxClient) transPool() interface{} {
	tr := *t.trans
	return &tr
}

func (t *CtxClient) Auth(user, pass string) {
	t.user = user
	t.pass = pass
}

// Call invokes SOAP method call, waits for it to complete, and returns its error status
// and raw http request and response. It receives context and can be safely canceled.
func (t *CtxClient) Call(ctx context.Context, in, out *Envelope) (error, []byte, []byte) {
	var (
		err  error
		xreq []byte
		xres []byte
	)
	b := buffPool.Get().(*bytes.Buffer)
	defer func() {
		b.Reset()
		buffPool.Put(b)
	}()
	if err = xml.NewEncoder(b).Encode(in); err != nil {
		return err, xreq, xres
	}
	req, err := http.NewRequest("POST", t.url, b)
	if err != nil {
		return err, xreq, xres
	}
	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if action := ctxValue(ctx, &actionCtx); action != "" {
		req.Header.Add("SOAPAction", action)
	}
	if t.user != "" {
		req.SetBasicAuth(t.user, t.pass)
	}
	xreq, _ = httputil.DumpRequest(req, true)
	trans := transPool.Get().(*http.Transport)
	defer transPool.Put(trans)
	client := &http.Client{Transport: trans}
	c := make(chan error)
	go func() {
		c <- func(res *http.Response, err error) error {
			if err != nil {
				return err
			}
			defer res.Body.Close()
			xres, _ = httputil.DumpResponse(res, true)
			switch res.StatusCode {
			case 200:
				// OK
			case 500:
				// Fault
			default:
				return fmt.Errorf("soap: unexpected HTTP response: %s", res.Status)
			}
			if res.ContentLength == 0 {
				return fmt.Errorf("soap: error document empty")
			}
			if err := xml.NewDecoder(res.Body).Decode(out); err != nil {
				return err
			}
			ioutil.ReadAll(res.Body)
			return nil
		}(client.Do(req))
	}()
	select {
	case <-ctx.Done():
		trans.CancelRequest(req)
		<-c
		return ctx.Err(), xreq, xres
	case err := <-c:
		return err, xreq, xres
	}
	return nil, xreq, xres
}

var actionCtx = "holds action"

// Action returns new context containing soap action
func (t *CtxClient) Action(ctx context.Context, action string) context.Context {
	return context.WithValue(ctx, &actionCtx, action)
}

func ctxValue(ctx context.Context, key interface{}) string {
	s, ok := ctx.Value(key).(string)
	if ok == false {
		return ""
	}
	return s
}
