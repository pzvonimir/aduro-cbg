// Package soap implements a Simple Object Access Protocol (SOAP) 1.1
package v2

// References:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/
import (
	"encoding/xml"
	"fmt"
	"io"
)

// A Fault represents a SOAP fault:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383507.
type Fault struct {
	XMLName     xml.Name    `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`
	Faultcode   string      `xml:"faultcode"`
	Faultstring string      `xml:"faultstring"`
	Faultactor  string      `xml:"faultactor"`
	Detail      FaultDetail `xml:"detail"`
}

// Fault detail:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Ref477488700
type FaultDetail struct {
	Data string `xml:",innerxml"`
}

func (t *Fault) Error() string {
	return fmt.Sprintf("soap: %s[%s] %s details: %s", t.Faultactor, t.Faultcode, t.Faultstring, t.Detail.Data)
}

// A Header represents a SOAP header:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383497
type Header struct {
	EncodingStyle string `xml:"http://schemas.xmlsoap.org/soap/envelope/ encodingStyle,attr"`
	Header        interface{}
}

// UnmarshalXML decodes Header element
func (t *Header) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	return decode(t.Header, d)
}

// NewHeader creates a new Header element containing v
func NewHeader(v interface{}) *Header {
	return &Header{
		EncodingStyle: "http://schemas.xmlsoap.org/soap/encoding/",
		Header:        v,
	}
}

// A Body represents a SOAP body:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383503
type Body struct {
	EncodingStyle string `xml:"http://schemas.xmlsoap.org/soap/envelope/ encodingStyle,attr"`
	Body          interface{}
}

// UnmarshalXML decodes Body element
func (t *Body) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	return decode(t.Body, d)
}

// NewBody creates a new Body element containing v
func NewBody(v interface{}) *Body {
	return &Body{
		EncodingStyle: "http://schemas.xmlsoap.org/soap/encoding/",
		Body:          v,
	}
}

// A Envelope represents a SOAP envelope:
// http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383494
type Envelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  *Header  `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header,omitempty"`
	Body    *Body    `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
}

// NewEnvelope creates a new Envelope with body child b and header child h if h is defined
func NewEnvelope(b, h interface{}) *Envelope {
	t := &Envelope{
		Body: NewBody(b),
	}
	if h != nil {
		t.Header = NewHeader(h)
	}
	return t
}

// decodes a XML element into interface v
func decode(v interface{}, d *xml.Decoder) error {
	consumed := false
	for {
		tok, err := d.Token()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
		switch tok.(type) {
		case xml.StartElement:
			if consumed == false {
				e := tok.(xml.StartElement)
				switch e.Name.Local {
				case "Fault":
					f := &Fault{}
					if err := d.DecodeElement(f, &e); err != nil {
						return err
					}
					return f
				default:
					if err := d.DecodeElement(v, &e); err != nil {
						return err
					}
					consumed = true
				}
			}
		}
	}
	return nil
}
