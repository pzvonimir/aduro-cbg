// Copyright 2009 The Go Authors. All rights reserved.
// Copyright 2014 Aduro Ideja. All rights reserved.
// Use of this source code is governed by a BSD-style

package v2

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"net/http"
	"reflect"
	"sync"
	"unicode"
	"unicode/utf8"
)

type logger interface {
	Debugf(string, ...interface{})
	Errorf(string, ...interface{})
}

const (
	methodArg int = 1 + iota
	methodReply
	methodArgHead
	methodReplayHead
)

// Server type
type Server struct {
	hs       *http.Server
	log      logger
	mu       sync.Mutex
	services map[string]*serviceMap
}

// Service map
type serviceMap struct {
	rcvr   reflect.Value
	method map[string]*methodType
}

// Method type
type methodType struct {
	method reflect.Method
	argv   map[int]reflect.Type
	argc   int
}

// NewServer returns a new SOAP server.
func NewServer(hs *http.Server, log logger) *Server {
	if hs == nil {
		hs = &http.Server{Addr: ":8080"}
	}
	t := &Server{
		hs:       hs,
		log:      log,
		services: make(map[string]*serviceMap),
	}
	return t
}

// Listen and serve
func (t *Server) ListenAndServe() {
	t.log.Debugf("[soap] server started")
	if err := t.hs.ListenAndServe(); err != nil {
		t.log.Errorf("[soap] listen error: %v", err)
	}
}

// RegisterService adds a new service to the server. On path provided.
// Methods from the receiver will be extracted if these rules are satisfied:
//
//    - The receiver is exported
//    - The method name is exported.
//    - The method has two, three or four arguments: *args, *reply[, *argsHeader[, *replyHeader]].
//    - All arguments are pointers.
//    - The arguments are exported or local.
//    - The method has return type error.
//
// Other methods are ignored.
func (t *Server) Register(rcvr interface{}, paht string) error {
	t.mu.Lock()
	defer t.mu.Unlock()
	s := &serviceMap{
		rcvr:   reflect.ValueOf(rcvr),
		method: make(map[string]*methodType),
	}
	typ := reflect.TypeOf(rcvr)
	sname := reflect.Indirect(s.rcvr).Type().Name()
	if !isExported(sname) {
		return fmt.Errorf("[soap] type %q is not exported", sname)
	}
	if paht == "" {
		paht = fmt.Sprintf("/%s", sname)
	}
	t.log.Debugf("[soap] register service %s", paht)
	if _, in := t.services[paht]; in == true {
		return fmt.Errorf("[soap] service path %s already defined", paht)
	}
	// Install the methods
	for i := 0; i < typ.NumMethod(); i++ {
		m := &methodType{
			method: typ.Method(i),
			argv:   make(map[int]reflect.Type),
		}
		t.log.Debugf("[soap] installing method: %s", m.method.Name)
		// Method must be exported.
		if m.method.PkgPath != "" {
			continue
		}
		t.log.Debugf("[soap] method arguments: %d", m.method.Type.NumIn())
		// Method format: receiver, *args, *reply [, *argsHeader[, *replyHeader]].
		if m.method.Type.NumIn() < 3 || m.method.Type.NumIn() > 5 {
			continue
		}
		m.argc = m.method.Type.NumIn() - 1
		for j := 1; j < m.method.Type.NumIn(); j++ {
			argType := m.method.Type.In(j)
			if argType.Kind() != reflect.Ptr || !isExportedOrBuiltinType(argType) {
				continue
			}
			m.argv[j] = argType.Elem()
		}
		// Method needs one out.
		if m.method.Type.NumOut() != 1 {
			continue
		}
		// The return type of the method must be error.
		if returnType := m.method.Type.Out(0); returnType != typeOfError {
			continue
		}
		s.method[m.method.Name] = m
	}
	if len(s.method) == 0 {
		return fmt.Errorf("[soap] %s type has no exported methods of suitable type", sname)
	}
	t.services[paht] = s
	http.DefaultServeMux.HandleFunc(paht, t.serveHTTP)
	return nil
}

// serveHTTP
func (t *Server) serveHTTP(w http.ResponseWriter, r *http.Request) {
	var (
		service  *serviceMap
		ok       bool
		errValue []reflect.Value
	)
	w.Header().Set("Content-Type", "text/xml; charset=\"utf-8\"")
	if r.Method != "POST" {
		str := fmt.Sprintf("POST method required, received %s", r.Method)
		t.log.Debugf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusMethodNotAllowed)
		return
	}
	t.log.Debugf("[soap] request service %s", r.URL.Path)
	if service, ok = t.services[r.URL.Path]; ok == false {
		str := fmt.Sprintf("service %s not found", r.URL.Path)
		t.log.Debugf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusBadRequest)
		return
	}
	env := NewEnvelope(&soapRequest{}, &soapRequest{})
	if err := xml.NewDecoder(r.Body).Decode(env); err != nil {
		str := fmt.Sprintf("decode error: %v", err)
		t.log.Errorf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusInternalServerError)
		return
	}
	reqBody := env.Body.Body.(*soapRequest)
	if _, ok = service.method[reqBody.XMLName.Local]; ok == false {
		str := fmt.Sprintf("service %s method %s not found", r.URL.Path, reqBody.XMLName.Local)
		t.log.Debugf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusBadRequest)
		return
	}
	serviceCall := make([]reflect.Value, service.method[reqBody.XMLName.Local].argc+1)
	serviceCall[0] = service.rcvr
	// Decode the header.
	if service.method[reqBody.XMLName.Local].argc > 2 {
		serviceCall[methodArgHead] = reflect.New(service.method[reqBody.XMLName.Local].argv[methodArgHead])
		if err := xml.Unmarshal(buildElement(env.Header.Header.(*soapRequest)), serviceCall[methodArgHead].Interface()); err != nil {
			str := fmt.Sprintf("header decode error: %v", err)
			t.log.Errorf("[soap] %s", str)
			http.Error(w, errorEncode(str), http.StatusInternalServerError)
			return
		}
	}
	// Decode the args.
	serviceCall[methodArg] = reflect.New(service.method[reqBody.XMLName.Local].argv[methodArg])
	if err := xml.Unmarshal(buildElement(reqBody), serviceCall[methodArg].Interface()); err != nil {
		str := fmt.Sprintf("payload decode error: %v", err)
		t.log.Errorf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusInternalServerError)
		return
	}
	// Call the service method.
	serviceCall[methodReply] = reflect.New(service.method[reqBody.XMLName.Local].argv[methodReply])
	if service.method[reqBody.XMLName.Local].argc == 4 {
		serviceCall[methodReplayHead] = reflect.New(service.method[reqBody.XMLName.Local].argv[methodReplayHead])
	}
	errValue = service.method[reqBody.XMLName.Local].method.Func.Call(serviceCall)
	// Cast the result to error if needed.
	var errResult error
	if errInter := errValue[0].Interface(); errInter != nil {
		errResult = errInter.(error)
	}
	if errResult == nil {
		b := &bytes.Buffer{}
		var res *Envelope
		if service.method[reqBody.XMLName.Local].argc > 3 {
			res = NewEnvelope(serviceCall[methodReply].Interface(), serviceCall[methodReplayHead].Interface())
		} else {
			res = NewEnvelope(serviceCall[methodReply].Interface(), nil)
		}
		if err := xml.NewEncoder(b).Encode(res); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintln(w, b.String())
	} else {
		str := fmt.Sprintf("error: %v", errResult)
		t.log.Errorf("[soap] %s", str)
		http.Error(w, errorEncode(str), http.StatusInternalServerError)
		return
	}
}

// Precompute the reflect type for error.  Can't use error directly
// because Typeof takes an empty interface value.  This is annoying.
var typeOfError = reflect.TypeOf((*error)(nil)).Elem()

// Is this type exported or a builtin?
func isExportedOrBuiltinType(t reflect.Type) bool {
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	// PkgPath will be non-empty even for an exported type,
	// so we need to check the type name as well.
	return isExported(t.Name()) || t.PkgPath() == ""
}

// Is this an exported - upper case - name?
func isExported(name string) bool {
	rune, _ := utf8.DecodeRuneInString(name)
	return unicode.IsUpper(rune)
}

// Build soap element back
func buildElement(s *soapRequest) []byte {
	var buff string
	if s.XMLName.Space == "" {
		buff = fmt.Sprintf("<%[1]s>%[2]s</%[1]s>", s.XMLName.Local, s.Innerxml)
	} else {
		buff = fmt.Sprintf("<%[1]s xmlns=\"%[2]s\">%[3]s</%[1]s>", s.XMLName.Local, s.XMLName.Space, s.Innerxml)
	}
	return []byte(buff)
}

// Encode SOAP Fault
func errorEncode(err string) string {
	b := &bytes.Buffer{}
	res := NewEnvelope(&Fault{
		Faultcode:   "Server",
		Faultstring: err,
	}, nil)
	xml.NewEncoder(b).Encode(res)
	return b.String()
}

// SOAP request used for geting soap payload from SOAP body and header
type soapRequest struct {
	XMLName  xml.Name
	Innerxml string `xml:",innerxml"`
}
