// +build !windows

package log

import (
	"log"
	"log/syslog"
	"os"
)

func (t *Logger) initLogger() {
	switch logType {
	case "syslog":
		var err error
		t.log, err = syslog.NewLogger(syslog.LOG_INFO, 0)
		if err != nil {
			panic(err)

		}
	default:
		t.log = log.New(os.Stdout, "", 0)

	}
}
