// Copyright 2015 Aduro Ideja All rights reserved.

// Package log implements a logging package. It defines a type, Logger, with methods for formatting output.
package log

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/net/context"
)

type logger interface {
	Printf(format string, v ...interface{})
}

// Log priority
type Priority uint8

const (
	LOG_CRIT Priority = iota
	LOG_ERR
	LOG_WARNING
	LOG_INFO
	LOG_DEBUG
)

type message struct {
	ts       time.Time
	msg      string
	priority Priority
}

type Logger struct {
	log      logger
	priority Priority
	queue    chan message
	pid      int
	name     string
	done     chan struct{}
}

var logType string

var std *Logger

func init() {
	flag.StringVar(&logType, "log", "stdout",
		"Which log output will be used. Supported outputs: stdout, syslog")
	std = New(LOG_DEBUG)
}

func filename() string {
	d, n := filepath.Split(os.Args[0])
	return fmt.Sprintf("%s%s", d, n)
}

var ctxTracker = "holds tracker"

// Creates new context with provided tracking id
func Tracker(ctx context.Context, v interface{}) context.Context {
	return context.WithValue(ctx, &ctxTracker, v)
}

// Returns tracking id
func GetTracker(ctx context.Context) interface{} {
	if ctx == nil {
		return nil
	}
	return ctx.Value(&ctxTracker)
}

// New creates a new Logger. The priority is minimal level logged.
// The contextKey is key used to get value from context.
func New(priority Priority) *Logger {
	if std == nil {
		std = &Logger{
			pid:      os.Getpid(),
			name:     filename(),
			priority: priority,
			queue:    make(chan message, 1000),
			done:     make(chan struct{}),
		}
		std.initLogger()
		std.loop()
	}
	if std.priority != priority {
		std.Priority(priority)
	}
	return std
}

func (t *Logger) Priority(p Priority) {
	t.priority = p
}

func (t *Logger) loop() {
	go func() {
		for {
			select {
			case <-t.done:
				return
			case msg := <-t.queue:
				if msg.priority <= t.priority {
					t.log.Printf("%s %s[%d]: %s",
						msg.ts.Format("2006-01-02 15:04:05.000000"),
						t.name,
						t.pid,
						msg.msg,
					)
				}
			}
		}
	}()
}

// Closes log and flushes messages
func (t *Logger) Close() {
	for len(t.queue) != 0 {
		time.Sleep(10 * time.Millisecond)
	}
	close(t.done)
}

// Logf formats its arguments according to the format, analogous to fmt.Printf,
// and records the text as a log message at the Priority level.
// The message will be associated with the provided context.
func (t *Logger) Logf(ctx context.Context, p Priority, format string, v ...interface{}) {
	t.queue <- message{
		ts:       time.Now(),
		msg:      fmt.Sprintf("%v: %s", GetTracker(ctx), fmt.Sprintf(format, v...)),
		priority: p,
	}
}

// Debugf is like Logf, but at Info level.
func (t *Logger) Debugf(ctx context.Context, format string, v ...interface{}) {
	t.Logf(ctx, LOG_DEBUG, format, v...)
}

// Infof is like Logf, but at Info level.
func (t *Logger) Infof(ctx context.Context, format string, v ...interface{}) {
	t.Logf(ctx, LOG_INFO, format, v...)
}

// Warningf is like Logf, but at Info level.
func (t *Logger) Warningf(ctx context.Context, format string, v ...interface{}) {
	t.Logf(ctx, LOG_WARNING, format, v...)
}

// Errorf is like Logf, but at Info level.
func (t *Logger) Errorf(ctx context.Context, format string, v ...interface{}) {
	t.Logf(ctx, LOG_ERR, format, v...)
}

// Criticalf is like Logf, but at Info level.
func (t *Logger) Criticalf(ctx context.Context, format string, v ...interface{}) {
	t.Logf(ctx, LOG_CRIT, format, v...)
}

// Debugf is like Logf, but at Info level.
func Debugf(ctx context.Context, format string, v ...interface{}) {
	std.Debugf(ctx, format, v...)
}

// Infof is like Logf, but at Info level.
func Infof(ctx context.Context, format string, v ...interface{}) {
	std.Infof(ctx, format, v...)
}

// Warningf is like Logf, but at Info level.
func Warningf(ctx context.Context, format string, v ...interface{}) {
	std.Warningf(ctx, format, v...)
}

// Errorf is like Logf, but at Info level.
func Errorf(ctx context.Context, format string, v ...interface{}) {
	std.Errorf(ctx, format, v...)
}

// Criticalf is like Logf, but at Info level.
func Criticalf(ctx context.Context, format string, v ...interface{}) {
	std.Criticalf(ctx, format, v...)
}

// Change default priority level
func SetPriority(p Priority) {
	std.priority = p
}

// Closes log and flushes messages
func Close() {
	for len(std.queue) != 0 {
		time.Sleep(10 * time.Millisecond)
	}
	close(std.done)
}
