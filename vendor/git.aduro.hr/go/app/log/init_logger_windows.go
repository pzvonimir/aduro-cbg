package log

import (
	"log"
	"os"
)

func (t *Logger) initLogger() {
	switch logType {
	// removed syslog dependency
	default:
		t.log = log.New(os.Stdout, "", 0)

	}
}
