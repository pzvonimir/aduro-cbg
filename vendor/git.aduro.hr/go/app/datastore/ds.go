// Copyright 2015 Aduro Ideja All rights reserved.

// Package datastore provides a wrapper around database/sql package
// providing methods which are useful in the development of database driven applications.
// Additions include caching of prepared queries, scanning into structs,
// binding struct elements to query placeholders.
package datastore

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/context"
)

// Each exported struct field becomes bind parameter
// Examples:
// // Field is ignored by this package.
// Field int `ds:"-"`
// // Field is used for query functions
// Field int `ds:"query"`
// // Field is used for exec functions
// Field int `ds:"exec"`
// // Field is used for query and exec functions
// Field int
// Field int `ds:"query,exec"`
const (
	tag   = "ds"
	query = "query"
	exec  = "exec"
)

// Query id type used from query to stmt mapping
type QueryId uint8

// QueryLog function callback is called for every query execution
type QueryLog func(ctx context.Context, q string, args ...interface{})

var (
	// ErrorPointerExpected is returned when not pointer is send for query
	ErrorPointerExpected = errors.New("ds: pointer expected")
	// ErrorStructExpected is returned when struct is not received
	ErrorStructExpected = errors.New("ds: pointer to struct expected")
	// ErrorEmptySlice is returned when empty slice is received
	ErrorEmptySlice = errors.New("ds: empty slice received")
	// ErrorSliceExpected is returned when slice is not received
	ErrorSliceExpected = errors.New("ds: pointer to slice expected")
	// ErrorTransactionNotFound is returned when transaction is not found in context
	ErrorTransactionNotFound = errors.New("ds: transaction not found")
	// Holds transaction
	contextTxKey = "holds *sql.Tx"
)

// Get transaction from context, return ErrorTransactionNotFound if there is no transaction
func txFromCTx(ctx context.Context) (*sql.Tx, error) {
	tx, ok := ctx.Value(&contextTxKey).(*sql.Tx)
	if ok == false {
		return nil, ErrorTransactionNotFound
	}
	return tx, nil
}

func getStruct(t reflect.Type, ptr bool) (reflect.Value, reflect.Type, bool) {
	if k := t.Kind(); k != reflect.Struct {
		return getStruct(t.Elem(), k == reflect.Ptr)
	}
	return reflect.Indirect(reflect.New(t)), t, ptr
}

func checkField(check string, f reflect.StructField) bool {
	tag := f.Tag.Get(tag)
	return f.PkgPath == "" && (strings.Index(tag, check) >= 0 || len(tag) == 0)
}

// Database is a wrapper around sql.DB
type Database struct {
	conn      *sql.DB
	stmtCache bool
	smx       sync.RWMutex
	stmt      map[QueryId]*sql.Stmt
	qs        map[QueryId]string
	ql        QueryLog
	done      chan struct{}
	dbType    DatabaseType // MySQLDB OracleDB
}

// New database returns a new DB wrapper for a pre-existing *sql.DB.
// The queries map will be used for preparing of statements
// The queryLogger if provided will be called for every query execution
func NewDatabase(db *sql.DB, queries map[QueryId]string, queryLogger QueryLog) *Database {
	t := &Database{
		conn:      db,
		stmtCache: true,
		stmt:      make(map[QueryId]*sql.Stmt, len(queries)),
		qs:        queries,
		ql:        queryLogger,
		done:      make(chan struct{}),
	}
	return t
}

// Should we cache and reuse statements. Defaults to true
func (t *Database) StmtCache(b bool) {
	t.stmtCache = b
}

// Convert query "?" placeholder to ":%d" placeholder
func (t *Database) ConvertPlaceholders() {
	for id, query := range t.qs {
		cnt := strings.Count(query, "?")
		for i := 0; i < cnt; i++ {
			query = strings.Replace(query, "?", fmt.Sprintf(":%d", i), 1)
		}
		t.qs[id] = query
	}
}

// DBType set database type
// used for bulk insert statement build, if not set, MySQL type is used
func (t *Database) DBType(dbType DatabaseType) {
	t.dbType = dbType
}

// Keep alive executes db.Ping every minute.
func (t *Database) KeepAlive() {
	tt := time.Tick(time.Minute)
	go func() {
		for {
			select {
			case <-t.done:
				return
			case <-tt:
				t.conn.Ping()
			}
		}
	}()
}

// Close closes the database, releasing any open resources.
func (t *Database) Close() error {
	close(t.done)
	return t.conn.Close()
}

// Calls query log callback if provided
func (t *Database) queryLog(ctx context.Context, qid QueryId, args ...interface{}) {
	if t.ql != nil {
		t.ql(ctx, t.qs[qid], args...)
	}
}

// Get prepared statement from cache map if there is no prepared statement prepare it.
func (t *Database) getStmt(ctx context.Context, qid QueryId, f func(*sql.Stmt) error) error {
	t.smx.RLock()
	s, ok := t.stmt[qid]
	t.smx.RUnlock()
	if ok == false {
		t.smx.Lock()
		defer t.smx.Unlock()
		var err error
		if s, err = t.conn.Prepare(t.qs[qid]); err != nil {
			return err
		}
		// because of the stupid drivers
		if t.stmtCache == true {
			t.stmt[qid] = s
		}
	}
	if tx, _ := txFromCTx(ctx); tx != nil {
		s = tx.Stmt(s)
	}
	return f(s)
}

// Query executes a prepared query statement with the given arguments
// and returns the query results as a *Rows.
func (t *Database) Query(ctx context.Context, qid QueryId, args ...interface{}) (*sql.Rows, error) {
	var rows *sql.Rows
	t.queryLog(ctx, qid, args...)
	err := t.getStmt(ctx, qid, func(s *sql.Stmt) (err error) {
		rows, err = s.Query(args...)
		if t.stmtCache == false {
			s.Close()
		}
		return
	})
	return rows, err
}

// QueryRow executes a query that is expected to return at most one row.
// QueryRow always return a non-nil value. Errors are deferred until Row's Scan method is called.
func (t *Database) QueryRow(ctx context.Context, qid QueryId, args ...interface{}) (*sql.Row, error) {
	var row *sql.Row
	t.queryLog(ctx, qid, args...)
	err := t.getStmt(ctx, qid, func(s *sql.Stmt) (err error) {
		row = s.QueryRow(args...)
		if t.stmtCache == false {
			s.Close()
		}
		return
	})
	return row, err
}

// Exec executes a query without returning any rows.
// The args are for any placeholder parameters in the query.
func (t *Database) Exec(ctx context.Context, qid QueryId, args ...interface{}) (sql.Result, error) {
	var res sql.Result
	t.queryLog(ctx, qid, args...)
	err := t.getStmt(ctx, qid, func(s *sql.Stmt) (err error) {
		res, err = s.Exec(args...)
		if t.stmtCache == false {
			s.Close()
		}
		return
	})
	return res, err
}

// ExecSlice executes a query without returning any rows.
// The args represents slice of placeholder parameters in the query.
func (t *Database) ExecSlice(ctx context.Context, qid QueryId, args [][]interface{}) error {
	err := t.getStmt(ctx, qid, func(s *sql.Stmt) (err error) {
		for i := range args {
			t.queryLog(ctx, qid, args[i]...)
			if _, err = s.Exec(args[i]...); err != nil {
				return
			}
		}
		if t.stmtCache == false {
			s.Close()
		}
		return
	})
	return err
}

// Exec executes a query without returning any rows.
// All Struct elements will be binded as placeholder parameters in the query.
func (t *Database) ExecStruct(ctx context.Context, qid QueryId, v interface{}) (sql.Result, error) {
	r := reflect.ValueOf(v)
	if r.Kind() != reflect.Ptr {
		return nil, ErrorPointerExpected
	}
	r = reflect.Indirect(r)
	if r.Kind() != reflect.Struct {
		return nil, ErrorStructExpected
	}
	rt := reflect.TypeOf(v).Elem()
	var args []interface{}
	for i := 0; i < r.NumField(); i++ {
		if checkField(exec, rt.Field(i)) {
			args = append(args, r.Field(i).Interface())
		}
	}
	return t.Exec(ctx, qid, args...)
}

// QueryStruct executes a query that is expected to return at most one row,
// and copies the columns from the matched row into the parameters of struct.
func (t *Database) QueryStruct(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) (err error) {
	row, err := t.QueryRow(ctx, qid, args...)
	if err != nil {
		return err
	}
	r := reflect.ValueOf(v)
	if r.Kind() != reflect.Ptr {
		return ErrorPointerExpected
	}
	r = reflect.Indirect(r)
	if r.Kind() != reflect.Struct {
		return ErrorStructExpected
	}
	rt := reflect.TypeOf(v).Elem()
	var scans []interface{}
	for i := 0; i < r.NumField(); i++ {
		if checkField(query, rt.Field(i)) {
			scans = append(scans, r.Field(i).Addr().Interface())
		}
	}
	if err := row.Scan(scans...); err != nil {
		return err
	}
	return nil
}

// ExecStructs executes a query without returning any rows.
// All structs elements will be binded as placeholder parameters in the query.
// ExecStructs will execute one query for each struct in slice
func (t *Database) ExecStructs(ctx context.Context, qid QueryId, v interface{}) error {
	s := reflect.ValueOf(v)
	if s.Kind() != reflect.Slice {
		return ErrorSliceExpected
	}
	slen := s.Len()
	if slen == 0 {
		return ErrorEmptySlice
	}
	r, rt, ptr := getStruct(reflect.TypeOf(v), false)
	var ids []int
	for i := 0; i < r.NumField(); i++ {
		if checkField(exec, rt.Field(i)) {
			ids = append(ids, i)
		}
	}
	stlen := len(ids)
	args := make([][]interface{}, slen)
	for i := 0; i < slen; i++ {
		args[i] = make([]interface{}, stlen)
		str := s.Index(i)
		if ptr == true {
			str = s.Index(i).Elem()
		}
		for key, val := range ids {
			args[i][key] = str.Field(val).Addr().Interface()
		}
	}
	if err := t.ExecSlice(ctx, qid, args); err != nil {
		return err
	}
	return nil
}

// QueryStructs executes a query that is expected to return at most one row,
// and copies the columns from the matched row into the parameters of structs received in slice.
func (t *Database) QueryStructs(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) (err error) {
	rows, err := t.Query(ctx, qid, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	s := reflect.ValueOf(v)
	if s.Kind() != reflect.Ptr {
		return ErrorPointerExpected
	}
	s = reflect.Indirect(s)
	if s.Kind() != reflect.Slice {
		return ErrorSliceExpected
	}
	r, rt, ptr := getStruct(reflect.TypeOf(v), false)
	var ids []int
	for i := 0; i < r.NumField(); i++ {
		if checkField(query, rt.Field(i)) {
			ids = append(ids, i)
		}
	}
	scans := make([]interface{}, len(ids))
	for rows.Next() {
		row := reflect.Indirect(reflect.New(r.Type()))
		for key, val := range ids {
			scans[key] = row.Field(val).Addr().Interface()
		}
		if err := rows.Scan(scans...); err != nil {
			return err
		}
		if ptr == true {
			s.Set(reflect.Append(s, row.Addr()))
		} else {
			s.Set(reflect.Append(s, row))
		}
	}
	return nil
}

// QueryVars executes a query that returns rows, and copies the columns
// from the matched row into the elements of received slice.
func (t *Database) QueryVars(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) (err error) {
	rows, err := t.Query(ctx, qid, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	s := reflect.ValueOf(v)
	if s.Kind() != reflect.Ptr {
		return ErrorPointerExpected
	}
	s = reflect.Indirect(s)
	if s.Kind() != reflect.Slice {
		return ErrorSliceExpected
	}
	scan := reflect.New(reflect.TypeOf(v).Elem().Elem()).Interface()
	for rows.Next() {
		if err := rows.Scan(scan); err != nil {
			return err
		}
		s.Set(reflect.Append(s, reflect.Indirect(reflect.ValueOf(scan))))
	}
	return nil
}

// QueryVar executes a query that returns row, and copies the column
// from the matched row into the var.
func (t *Database) QueryVar(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) (err error) {
	row, err := t.QueryRow(ctx, qid, args...)
	if err != nil {
		return err
	}
	return row.Scan(v)
}

// Begin starts a transaction.
// Begin creates new context containing transaction data.
// When transaction is committed or rolled back this context should be discarded
func (t *Database) Begin(ctx context.Context) (context.Context, error) {
	tx, err := t.conn.Begin()
	if err != nil {
		return nil, err
	}
	return context.WithValue(ctx, &contextTxKey, tx), nil
}

// Commit commits the transaction.
func (t *Database) Commit(ctx context.Context) error {
	tx, err := txFromCTx(ctx)
	if err != nil {
		return err
	}
	return tx.Commit()
}

// Rollback aborts the transaction.
func (t *Database) Rollback(ctx context.Context) error {
	tx, err := txFromCTx(ctx)
	if err != nil {
		return err
	}
	return tx.Rollback()
}
