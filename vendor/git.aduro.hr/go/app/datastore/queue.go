package datastore

import (
	"sync"

	"golang.org/x/net/context"
)

type Queue struct {
	ds            *Database
	execQueue     chan *request
	queryOneQueue chan *request
	queryQueue    chan *request
	wg            sync.WaitGroup
	stop          chan struct{}
}

func NewQueue(ds *Database) *Queue {
	t := &Queue{
		execQueue:     make(chan *request, 1000),
		queryOneQueue: make(chan *request, 1000),
		queryQueue:    make(chan *request, 1000),
		stop:          make(chan struct{}),
		ds:            ds,
	}
	go t.goExec()
	go t.goQuery()
	go t.goQueryOne()
	return t
}

func (t *Queue) Close() {
	t.wg.Wait()
	close(t.stop)
	t.ds.Close()
}

func (t *Queue) Insert(ctx context.Context, qid QueryId, v interface{}) error {
	t.wg.Add(1)
	defer t.wg.Done()
	r := &request{
		Ctx:     ctx,
		QueryId: qid,
		Value:   v,
		Done:    make(chan struct{}),
	}
	t.execQueue <- r
	<-r.Done
	return r.Error
}

func (t *Queue) Update(ctx context.Context, qid QueryId, v interface{}) error {
	t.wg.Add(1)
	defer t.wg.Done()
	r := &request{
		Ctx:     ctx,
		QueryId: qid,
		Value:   v,
		Done:    make(chan struct{}),
	}
	t.execQueue <- r
	<-r.Done
	return r.Error
}

func (t *Queue) Delete(ctx context.Context, qid QueryId, v interface{}) error {
	t.wg.Add(1)
	defer t.wg.Done()
	r := &request{
		Ctx:     ctx,
		QueryId: qid,
		Value:   v,
		Done:    make(chan struct{}),
	}
	t.execQueue <- r
	<-r.Done
	return r.Error
}

func (t *Queue) Select(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) error {
	t.wg.Add(1)
	defer t.wg.Done()
	r := &request{
		Ctx:     ctx,
		QueryId: qid,
		Value:   v,
		Args:    args,
		Done:    make(chan struct{}),
	}
	t.queryQueue <- r
	<-r.Done
	return r.Error
}

func (t *Queue) SelectOne(ctx context.Context, qid QueryId, v interface{}, args ...interface{}) error {
	t.wg.Add(1)
	defer t.wg.Done()
	r := &request{
		Ctx:     ctx,
		QueryId: qid,
		Value:   v,
		Args:    args,
		Done:    make(chan struct{}),
	}
	t.queryOneQueue <- r
	<-r.Done
	return r.Error
}

func (t *Queue) goExec() {
	go func() {
		for {
			select {
			case <-t.stop:
				return
			case req := <-t.execQueue:
				_, req.Error = t.ds.ExecStruct(req.Ctx, req.QueryId, req.Value)
				close(req.Done)
			}
		}
	}()
}

func (t *Queue) goQueryOne() {
	go func() {
		for {
			select {
			case <-t.stop:
				return
			case req := <-t.queryOneQueue:
				req.Error = t.ds.QueryStruct(req.Ctx, req.QueryId, req.Value, req.Args...)
				close(req.Done)
			}
		}
	}()
}

func (t *Queue) goQuery() {
	go func() {
		for {
			select {
			case <-t.stop:
				return
			case req := <-t.queryQueue:
				req.Error = t.ds.QueryStructs(req.Ctx, req.QueryId, req.Value, req.Args...)
				close(req.Done)
			}
		}
	}()
}

type request struct {
	Ctx     context.Context
	QueryId QueryId
	Value   interface{}
	Args    []interface{}
	Error   error
	Done    chan struct{}
}
