package helper

import (
	"git.aduro.hr/go/app/log"
	"golang.org/x/net/context"
)

func QueryLoggerFunc(ctx context.Context, q string, args ...interface{}) {
	log.Debugf(ctx, "%s: %v", q, args)
}
