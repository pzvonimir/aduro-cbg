package helper

import (
	"reflect"

	"golang.org/x/net/context"
)

// Precompute the reflect type for context.
var TypeOfContext = reflect.TypeOf((*context.Context)(nil)).Elem()
