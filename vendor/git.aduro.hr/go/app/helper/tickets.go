package helper

import (
	"errors"
	"sync"
	"time"
)

var (
	ErrTimeout = errors.New("tickets pool timeout")
)

// TicketsPool is used for limiting actions which can be done in parallel
// If all tickets are issued, others will have to wait before tickets are available
// Tickets don't carry any data on them, they are just permission to continue with the execution
type TicketsPool struct {
	pool      chan struct{}
	timerPool sync.Pool
	timeout   time.Duration
}

func NewTicketsPool(limit uint8, timeout time.Duration) *TicketsPool {
	tp := &TicketsPool{
		pool: make(chan struct{}, limit),
		timerPool: sync.Pool{
			New: func() interface{} { return time.NewTimer(timeout) },
		},
		timeout: timeout,
	}

	for i := uint8(0); i < limit; i++ {
		tp.pool <- struct{}{}
	}
	return tp
}

// Get the ticket in reasonable time, if no tickets are available timeout error is returned
func (t *TicketsPool) Get() error {
	timer := t.timerPool.Get().(*time.Timer)
	defer func() {
		timer.Stop()
		t.timerPool.Put(timer)
	}()
	timer.Reset(t.timeout)
	select {
	case <-t.pool:
		return nil
	case <-timer.C:
		return ErrTimeout
	}
}

// Release returns ticket to the pool
func (t *TicketsPool) Release() {
	t.pool <- struct{}{}
}
