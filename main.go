package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"

	"bitbucket.org/aduroidea/aduro-cbg/internal/server"
	"git.aduro.hr/go/app/log"
)

func main() {
	ctx := log.Tracker(context.Background(), "[service]")
	log.Infof(ctx, "Starting Aduro Cbg API service")

	var (
		aiwsEndpoint string
		aiwsUsername string
		aiwsPassword string
		dsn          string
		port         string
		timeout      int
		aiwsLog      bool
	)

	flag.StringVar(&aiwsEndpoint, "endpoint", "http://localhost:8086", "AIWS endpoint")
	flag.StringVar(&aiwsUsername, "username", "aduro", "AIWS username")
	flag.StringVar(&aiwsPassword, "password", "123", "AIWS password")
	flag.IntVar(&timeout, "timeout", 45, "AIWS Timeout in seconds")
	flag.BoolVar(&aiwsLog, "l", true, "Log AIWS SOAP calls")
	flag.StringVar(&port, "port", "8989", "API Service port")
	flag.StringVar(&dsn, "dsn", "root:root@tcp(localhost:3306)/otc?parseTime=true", "MySQL dsn")
	flag.Parse()

	// logger := log.New(log.LOG_INFO)

	srv, err := server.New(timeout)
	if err != nil {
		panic(err)
	}

	log.Infof(ctx, "ListenAndServe port: %s", port)

	for k, v := range srv.Handlers {
		log.Infof(ctx, `Init handler: "%s" Path: "%s"`, k, v.Path)
		http.Handle(v.Path, v.Func)
	}

	err = http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		panic(err)
	}
}
