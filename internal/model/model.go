package model

import "encoding/xml"

//Request

type Envelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	// Header  *Header  `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header,omitempty"`
	Body *Body `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
}

// type Header struct {
// 	EncodingStyle string `xml:"http://schemas.xmlsoap.org/soap/envelope/ encodingStyle,attr"`
// 	Header        interface{}
// }

type Body struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
	Call    *Call    `xml: "urn:/T2api/Proto/Soap"`
}

type Call struct {
	XMLName xml.Name `xml:"urn:/T2api/Proto/Soap Call"`
	Request Request  `xml:"urn:/T2api/Proto/Soap request"`
}

type Request struct {
	Url    string  `xml:"urn:/T2api/Proto/Soap url"`
	Method string  `xml:"urn:/T2api/Proto/Soap method"`
	Kwargs *Kwargs `xml:"urn:/T2api/Proto/Soap kwargs"`
}

type Kwargs struct {
	Item []*Item `xml:"urn:/T2api/Proto/Soap item"`
}

type Item struct {
	Key           string `xml:"urn:/T2api/Proto/Soap key"`
	ValueUnsigned int64  `xml:"urn:/T2api/Proto/Soap valueUnsigned"`
	ValueString   string `xml:"urn:/T2api/Proto/Soap valueString"`
}

// Response
type ResponseEnvelope struct {
	XMLName  xml.Name  `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	RespBody *RespBody `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
}

type RespBody struct {
	XMLName  xml.Name  `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
	Response *Response `xml:"urn:/T2api/Proto/Soap Response"`
}

type Response struct {
	Rc   int   `xml:"urn:/T2api/Proto/Soap rc"`
	Data *Data `xml:"urn:/T2api/Proto/Soap data"`
}

type Data struct {
	RespItem []*RespItem `xml:"urn:/T2api/Proto/Soap item"`
}

type RespItem struct {
	Key         string     `xml:"urn:/T2api/Proto/Soap key"`
	ValueString string     `xml:"urn:/T2api/Proto/Soap valueString"`
	ValueDict   *ValueDict `xml:"urn:/T2api/Proto/Soap valueDict"`
}

type ValueDict struct {
	Item []*Item `xml:"urn:/T2api/Proto/Soap item"`
}

type Charging struct {
	Version               int64
	ContentType           int64
	Currency              int64
	Amount                int64
	Vat                   int64
	OriginatingCustomerId string
	Username              string
	Password              string
	ContentDescription    string
	ProviderTransactionId int64
	ReferenceID           int64
	XtraData              string
}

type GetToken struct {
	Version               int64
	OriginatingCustomerIP string
	Username              string
	Password              string
}

type GetPhoneModel struct {
	Version  int64
	Msisdn   string
	Username string
	Password string
}

type GetImei struct {
	Version  int64
	Msisdn   string
	Username string
	Password string
}

type TranslateIp struct {
	Version               int64
	OriginatingCustomerIP string
	Username              string
	Password              string
}
