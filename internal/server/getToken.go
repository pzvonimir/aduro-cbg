package server

import (
	"encoding/xml"
	"net/http"

	"bitbucket.org/aduroidea/aduro-cbg/internal/model"
)

//ProcessGetToken method
func (t *Server) ProcessGetToken(w http.ResponseWriter, r *http.Request, soapReq model.Request) {
	var out model.ResponseEnvelope

	var getToken model.GetToken

	for _, item := range soapReq.Kwargs.Item {
		if len(item.Key) > 0 && item.Key == "Version" {
			getToken.Version = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "OriginatingCustomerIP" {
			getToken.OriginatingCustomerIP = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "username" {
			getToken.Username = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "password" {
			getToken.Password = item.ValueString
		}
	}

	if getToken.Version == 208 && getToken.OriginatingCustomerIP == "127.1.5.200" && getToken.Username == "K123456" && getToken.Password == "XXXXXXX" {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "TOKEN",
			ValueString: "UkIv0jElh3KO1SX8bKK2MaJLqOT8O89Ay3Msnb+ykTPVR/eT0xRybEKyZpTSCJfh7iq9maSKeBxqTcbkrbHXbKYA5us8fX7N5Zd7L/jdU6E+ymiwGGpEaaOkEw8WXvqr1l8J9zwvIHs9DoAAgyNIIFP+F7WO4CGEcitV09kl1oKe3B2nt71DIA==",
		}

		respItems = append(respItems, item)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 200,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	} else {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "error_code",
			ValueString: "ObjectNotFound",
		}

		item2 := &model.RespItem{
			Key:         "error_message",
			ValueString: "Object not found",
		}

		respItems = append(respItems, item)
		respItems = append(respItems, item2)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 452,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	}

	x, _ := xml.Marshal(out)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(x)
}
