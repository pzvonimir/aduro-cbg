package server

import (
	"context"
	"encoding/xml"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/aduroidea/aduro-cbg/internal/model"

	"io/ioutil"

	"git.aduro.hr/go/app/log"
)

// Server srvr
type Server struct {
	ctx      context.Context
	timeout  time.Duration
	Handlers map[string]Handler
}

type Handler struct {
	Path string
	Func http.Handler
}

// New init
func New(tm int) (*Server, error) {

	t := &Server{
		ctx:     log.Tracker(context.Background(), "[server]"),
		timeout: time.Duration(tm) * time.Second,
	}

	handlers := map[string]Handler{
		"main": Handler{Path: "/", Func: http.HandlerFunc(t.Process)},
	}
	t.Handlers = handlers

	return t, nil
}

// Process method
func (t *Server) Process(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	xmlData, err := ioutil.ReadAll(r.Body)

	if err != nil {
		fmt.Println("Error reading XML data:", err)
	}

	var xmlEnvelope model.Envelope
	xml.Unmarshal(xmlData, &xmlEnvelope)

	soapReq := xmlEnvelope.Body.Call.Request
	log.Infof(t.ctx, `aduroCbg Request: %+v`, soapReq)

	if r.Method == "GET" || r.Method == "POST" {

		if len(soapReq.Url) != 0 && len(soapReq.Method) != 0 {
			if soapReq.Url == "CBG" {
				switch soapReq.Method {
				case "Purchase":
					t.ProcessChargingAndReversal(w, r, soapReq)
				case "GetToken":
					t.ProcessGetToken(w, r, soapReq)
				case "GetPhoneModel":
					t.ProcessGetPhoneModel(w, r, soapReq)
				case "GetIMEI":
					t.ProcessGetIMEI(w, r, soapReq)
				case "TranslateIP":
					t.ProcessTranslateIp(w, r, soapReq)
				default:
					t.ProcessMethodUnknown(w, r, soapReq)
				}
			}
		}
	}

}

//ProcessMethodUnknown method
func (t *Server) ProcessMethodUnknown(w http.ResponseWriter, r *http.Request, soapReq model.Request) {
	var out model.ResponseEnvelope
	var respItems []*model.RespItem

	item := &model.RespItem{
		Key:         "rc_string",
		ValueString: "MethodNotFound",
	}

	item2 := &model.RespItem{
		Key:         "rc_message",
		ValueString: "Method not found",
	}

	respItems = append(respItems, item)
	respItems = append(respItems, item2)

	out.RespBody = &model.RespBody{
		Response: &model.Response{
			Rc: 400,
			Data: &model.Data{
				RespItem: respItems,
			},
		},
	}

	x, _ := xml.Marshal(out)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(x)
}
