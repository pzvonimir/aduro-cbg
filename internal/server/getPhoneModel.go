package server

import (
	"encoding/xml"
	"net/http"

	"bitbucket.org/aduroidea/aduro-cbg/internal/model"
)

//ProcessGetPhoneModel method
func (t *Server) ProcessGetPhoneModel(w http.ResponseWriter, r *http.Request, soapReq model.Request) {
	var out model.ResponseEnvelope

	var getPhoneModel model.GetPhoneModel

	for _, item := range soapReq.Kwargs.Item {
		if len(item.Key) > 0 && item.Key == "Version" {
			getPhoneModel.Version = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "MSISDN" {
			getPhoneModel.Msisdn = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "username" {
			getPhoneModel.Username = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "password" {
			getPhoneModel.Password = item.ValueString
		}
	}

	if getPhoneModel.Version == 208 && getPhoneModel.Msisdn == "0046704123456" && getPhoneModel.Username == "K123456" && getPhoneModel.Password == "VerySecretPassword" {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "PHONEMODEL",
			ValueString: "SonyEricsson S500i/SonyEricsson S500c",
		}

		respItems = append(respItems, item)

		item2 := &model.RespItem{
			Key:         "rc_message",
			ValueString: "",
		}

		respItems = append(respItems, item2)

		item3 := &model.RespItem{
			Key:         "rc_string",
			ValueString: "Success",
		}

		respItems = append(respItems, item3)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 200,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	} else {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "rc_string",
			ValueString: "ObjectNotFound",
		}

		item2 := &model.RespItem{
			Key:         "rc_message",
			ValueString: "Subscriber not found",
		}

		item3 := &model.RespItem{
			Key:         "error_message",
			ValueString: "Subscriber not found",
		}

		item4 := &model.RespItem{
			Key:         "error_code",
			ValueString: "ObjectNotFound",
		}

		respItems = append(respItems, item)
		respItems = append(respItems, item2)
		respItems = append(respItems, item3)
		respItems = append(respItems, item4)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 452,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	}

	x, _ := xml.Marshal(out)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(x)
}
