package server

import (
	"encoding/xml"
	"net/http"

	"bitbucket.org/aduroidea/aduro-cbg/internal/model"
)

//ProcessTranslateIp method
func (t *Server) ProcessTranslateIp(w http.ResponseWriter, r *http.Request, soapReq model.Request) {
	var out model.ResponseEnvelope

	var translateIp model.TranslateIp

	for _, item := range soapReq.Kwargs.Item {
		if len(item.Key) > 0 && item.Key == "Version" {
			translateIp.Version = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "OriginatingCustomerIP" {
			translateIp.OriginatingCustomerIP = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "username" {
			translateIp.Username = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "password" {
			translateIp.Password = item.ValueString
		}
	}

	if translateIp.Version == 208 && translateIp.OriginatingCustomerIP == "123.45.67.87" && translateIp.Username == "K010101" && translateIp.Password == "SecretPassword" {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "OriginatingCustomerId",
			ValueString: "00460704123456",
		}

		respItems = append(respItems, item)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 200,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	} else {
		var respItems []*model.RespItem

		item3 := &model.RespItem{
			Key:         "error_message",
			ValueString: "Subscriber not found",
		}

		item4 := &model.RespItem{
			Key:         "error_code",
			ValueString: "ObjectNotFound",
		}

		respItems = append(respItems, item3)
		respItems = append(respItems, item4)

		out.RespBody = &model.RespBody{
			Response: &model.Response{
				Rc: 452,
				Data: &model.Data{
					RespItem: respItems,
				},
			},
		}

	}

	x, _ := xml.Marshal(out)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(x)
}
