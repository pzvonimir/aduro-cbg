package server

import (
	"encoding/xml"
	"net/http"

	"bitbucket.org/aduroidea/aduro-cbg/internal/model"
)

//ProcessChargingAndReversal method
func (t *Server) ProcessChargingAndReversal(w http.ResponseWriter, r *http.Request, soapReq model.Request) {
	var charging model.Charging

	for _, item := range soapReq.Kwargs.Item {
		if len(item.Key) > 0 && item.Key == "Version" {
			charging.Version = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "ContentType" {
			charging.ContentType = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "Currency" {
			charging.Currency = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "Amount" {
			charging.Amount = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "VAT" {
			charging.Vat = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "OriginatingCustomerId" {
			charging.OriginatingCustomerId = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "username" {
			charging.Username = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "password" {
			charging.Password = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "ContentDescription" {
			charging.ContentDescription = item.ValueString
		}

		if len(item.Key) > 0 && item.Key == "ProviderTransactionId" {
			charging.ProviderTransactionId = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "ReferenceID" {
			charging.ReferenceID = item.ValueUnsigned
		}

		if len(item.Key) > 0 && item.Key == "XtraData" {
			charging.XtraData = item.ValueString
		}
	}

	if charging.Version == 208 && charging.ContentType == 1 && charging.Currency == 1 && charging.Amount == 100 && charging.Vat == 2500 &&
		charging.OriginatingCustomerId == "0046704123456" && charging.Username == "K010101" && charging.Password == "SecretPassword" &&
		charging.ContentDescription == "ProviderDefinedText" && charging.ProviderTransactionId == 1234 && charging.ReferenceID == 0 &&
		charging.XtraData == "Provider defined text" {

		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "rc_message",
			ValueString: "",
		}

		respItems = append(respItems, item)

		insideItem := &model.Item{
			Key:         "TransactionId",
			ValueString: "123456789",
		}

		insideItem2 := &model.Item{
			Key:           "Status",
			ValueUnsigned: 0,
		}

		var items []*model.Item

		items = append(items, insideItem)
		items = append(items, insideItem2)

		item2 := &model.RespItem{
			Key: "CBGRESPONSE",
			ValueDict: &model.ValueDict{
				Item: items,
			},
		}

		respItems = append(respItems, item2)

		item3 := &model.RespItem{
			Key:         "rc_string",
			ValueString: "Success",
		}

		respItems = append(respItems, item3)

		out := &model.ResponseEnvelope{
			RespBody: &model.RespBody{
				Response: &model.Response{
					Rc: 200,
					Data: &model.Data{
						RespItem: respItems,
					},
				},
			},
		}

		x, _ := xml.Marshal(out)
		w.Header().Set("Content-Type", "application/xml")
		w.Write(x)
	} else {
		var respItems []*model.RespItem

		item := &model.RespItem{
			Key:         "rc_message",
			ValueString: "ERROR",
		}

		respItems = append(respItems, item)

		insideItem := &model.Item{
			Key:         "TransactionId",
			ValueString: "",
		}

		insideItem2 := &model.Item{
			Key:           "Status",
			ValueUnsigned: 0,
		}

		var items []*model.Item

		items = append(items, insideItem)
		items = append(items, insideItem2)

		item2 := &model.RespItem{
			Key: "CBGRESPONSE",
			ValueDict: &model.ValueDict{
				Item: items,
			},
		}

		respItems = append(respItems, item2)

		item3 := &model.RespItem{
			Key:         "rc_string",
			ValueString: "Error",
		}

		respItems = append(respItems, item3)

		out := &model.ResponseEnvelope{
			RespBody: &model.RespBody{
				Response: &model.Response{
					Rc: 452,
					Data: &model.Data{
						RespItem: respItems,
					},
				},
			},
		}

		x, _ := xml.Marshal(out)
		w.Header().Set("Content-Type", "application/xml")
		w.Write(x)
	}
}
