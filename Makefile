.DEFAULT_GOAL: install

PWD=$(shell pwd)
NAME=$(shell basename $(PWD))
GOSRC=$(GOPATH)/src/
VERSION=$(shell git rev-parse HEAD)
BUILD_TIME=$(shell date +%FT%T%z)
LDFLAGS=-ldflags "-X bitbucket/aduroidea/aduro-cbg/bin/app.Version=${VERSION} -X bitbucket/aduroidea/aduro-cbg/bin/app.BuildTime=${BUILD_TIME}"

install: main.go
	go install ${LDFLAGS} -v

.PHONY: clean
clean:
	go clean -x
	rm -vf ${GOPATH}/bin/${NAME}
	rm -vrf $(subst src,pkg/linux_amd64,$(PWD))
